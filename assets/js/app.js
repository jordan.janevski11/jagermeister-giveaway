import * as jsHelpers from "./helper.js";

$(function () {
	let attempts = parseInt(localStorage.getItem("attempts")) || 0;

	const modalContainer = $(".modal");
	const formModal = $("#participationModal");
	const responseMsg = $("#responseMsg");
	const myForm = $("#myForm");
	const emailErrorMsg = $("#emailErrorMsg");
	const imageErrorMsg = $("#imageErrorMsg");

	jsHelpers.slideShow();

	$(document).on("click", ".modal-close, .modal-background", function (e) {
		modalContainer.hide();
		jsHelpers.clearErrors();
	});

	$(document).on("click", "#participate", function (e) {
		jsHelpers.isRestricted();
		formModal.show();
		responseMsg.hide();
		modalContainer.css({ display: "flex" });
		jsHelpers.animateCSS("#participationModal", "backInDown");
	});

	$("#image").on("change", function (e) {
		$(".file-name").show();
		$(".file-name").text($("#image")[0].files[0].name);
	});

	if (!jsHelpers.isRestricted()) {
		myForm.on("submit", handleSubmit);
	} else {
		myForm.on("submit", function (e) {
			e.preventDefault();
		});
	}

	function handleSubmit(e) {
		e.preventDefault();
		let errors = [];
		let email = $("#email").val();
		let file_data = $("#image").prop("files")[0];
		jsHelpers.clearErrors();

		if (!jsHelpers.validateEmail(email)) {
			errors.push("#email");
			emailErrorMsg.text("Please enter a valid email address");
			$("#email").addClass("myError");
		}
		if (!file_data || !jsHelpers.validateFile(file_data)) {
			errors.push("#fakeImageInput");
			$("#fakeImageInput").addClass("myError");
			imageErrorMsg.text("Image upload suports only .jpg/jpeg, .png, .svg files not over 4Mb");
		}

		if (errors.length > 0) {
			jsHelpers.animateCSS("#participationModal", "shakeX");
			return -1;
		}

		$("#progress").width("0px");
		$("#progressBar").css({ display: "flex" });

		let form_data = new FormData();
		form_data.append("file", file_data);
		form_data.append("email", email);

		$.ajax({
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener(
					"progress",
					function (e) {
						if (e.lengthComputable) {
							var percentComplete = (e.loaded / e.total) * 100;
							$("#progress").width(percentComplete + "%");
						}
					},
					false
				);
				return xhr;
			},
			url: "actions/submitForm.php",
			type: "POST",
			dataType: "script",
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
		})
			.then((response) => {
				if (response == 0 || response == 4) {
					attempts++;
					let msg = "";
					if (attempts == 3) {
						msg = `Image is not a receipt. This is your 3rd invalid request therefore you are banned for the next 24 hours!`;
					} else {
						msg = `Image is not a receipt. ${
							3 - attempts
						} out of 3 bad attempts left untill you are banned for the next 24 hours!`;
					}
					jsHelpers.animateModalWithFollowUpMsg(msg);
					localStorage.setItem("attempts", attempts);
					if (attempts == 3) {
						$("#mySubmitBtn").attr("disabled", true);
						$("#mySubmitBtn").css({ border: "3px solid red" });
						jsHelpers.restrictUser();
					}
				} else if (response == 1) {
					jsHelpers.animateModalWithFollowUpMsg(
						"Successful participation, you will recieve an email with additional information. Wish you the best of luck!!!"
					);
				} else if (response == 2 || response == 3) {
					jsHelpers.animateModalWithFollowUpMsg("Issue with the service, please try again later");
				}
			})
			.fail((err) => {
				jsHelpers.animateModalWithFollowUpMsg("Issue with the service, please try again later");
			});
	}
});
