import * as helpers from "./helper.js";

export function print(object) {
	$.post("actions/getReceipts.php", object)
		.then((data) => {
			if (object.status) {
				handleResponse(data, 2);
			} else if (object.action == "getPrizes") {
				handleResponse(data, 3);
			} else if (object.action == "getRandom") {
				handleResponse(data, 4);
			} else {
				handleResponse(data, 1);
			}
		})
		.catch((err) => failedRequest(err));
}

export function proccessAction(object) {
	$.post("actions/getReceipts.php", object)
		.then((data) => {
			handleActionResponse(data, object.msg, object.id);
		})
		.catch((err) => failedRequest(err));
}

//Populate modals

export function printInModal(id) {
	$.post("actions/getReceipts.php", { action: "getReceiptById", id: id })
		.then((data) => {
			let encryptedId = localStorage.getItem("currentAwardingProcces");
			if (data == 1) {
				return -1;
			}
			$("html").css({ overflow: "hidden" });
			$("#proceedToPrizes").attr("awardid", encryptedId);
			$("#givePrize").attr("awardid", encryptedId);
			$("#stepOne .modal-card-title").html(
				`Steps: <i class="fas fa-circle"></i> <i class="far fa-circle"></i>`
			);
			$("#stepTwo .modal-card-title").html(
				`Steps: <i class="fas fa-check-circle has-text-success"></i> <i class="fas fa-circle"></i>`
			);
			$("#stepOne .modal-card-body").html(
				`<p class="image is=4by3">
					<img src="${data.url}" alt="modalImg">
				</p>
				<p class="py-3"> Submited by : <b> ${data.email} </b> </p>
				<div class="infoContainerWithScroll">  
				<p> ${data.info} </p>				
				</div>`
			);
			$(".modal").css({ display: "flex" });
			$("#stepOne").css({ display: "flex" });
			$("#stepTwo").hide();
		})
		.catch((err) => failedRequest(err));
}

export function printPrizesInModal() {
	$.post("actions/getReceipts.php", { action: "getPrizes" })
		.then((data) => {
			if (data == 1) {
				return -1;
			}
			$("html").css({ overflow: "hidden" });
			$("#rewardsDropdown").html(`<option value="0">Pick a reward</option>`);
			data.forEach((element) => {
				if (element.quantity > 0) {
					$("#rewardsDropdown").append(
						$(
							`<option data-imgurl="${element.image}" value="${element.id}"> ${element.name} </option>`
						)
					);
				}
			});
		})
		.catch((err) => failedRequest(err));
}

function handleResponse(data, cardType) {
	$("#preview").html("");
	if (data == 1) {
		$("#preview").html("no receipts");
		return -1;
	}

	if (cardType == 1) {
		data.forEach((databaseRow) => {
			helpers.createCard(databaseRow).appendTo($("#preview"));
		});
	} else if (cardType == 2) {
		data.forEach((databaseRow) => {
			if (databaseRow.status == 1) {
				helpers.createAwardedCard(databaseRow).appendTo($("#preview"));
			} else {
				helpers.createRejectedCard(databaseRow).appendTo($("#preview"));
			}
		});
	} else if (cardType == 3) {
		data.forEach((databaseRow) => {
			helpers.createPrizeCard(databaseRow).appendTo($("#preview"));
		});
	} else if (cardType == 4) {
		helpers.createCard(data).appendTo($("#preview"));
	}
	$(".mainFlip").css({ height: $(".card").height() });
}

function handleActionResponse(data, msg, id) {
	if (data == 2) {
		showSuccessNotification(msg);
		hideCard(id);
	} else {
		displayError();
	}
}

function showSuccessNotification(msg) {
	$(".successNotification").text(msg);
	$(".successNotification")
		.stop(true, true)
		.css({ display: "block" })
		.delay("2000")
		.fadeOut("slow");
}

function showErrorNotification(msg) {
	$(".errorNotification").text(msg);
	$(".errorNotification").stop(true, true).css({ display: "block" }).delay("2000").fadeOut("slow");
}

function hideCard(id) {
	let element = document.getElementById(id);
	$(element).animate({ width: 0 }, "fast", function () {
		$(element).hide();
	});
}

function displayError() {
	showErrorNotification("Issues with the last command please refresh and start again");
	$("#preview").html("");
	return -1;
}

function failedRequest(err) {
	alert(`Sorry, There was an issue with the request`);
	return -1;
}
