import * as helpers from "./helper.js";
import * as ajax from "./ajaxRequests.js";

$(function () {
	if ($(window).width() < 768) {
		$("#main-sidebar").addClass("closed");
		helpers.closeSideBar();
	}

	$(window).on("resize", function (e) {
		$(".mainFlip").css({ height: $(".card").height() });
		if ($(window).width() < 768) {
			$("#main-sidebar").addClass("closed");
			helpers.closeSideBar();
		}
	});

	$(document).on("click", "#sidebar-toggler", function (e) {
		$("#main-sidebar").toggleClass("closed");
		setTimeout(function () {
			$(".mainFlip").css({ height: $(".card").height() });
		}, 200);

		if ($("#main-sidebar").hasClass("closed")) {
			helpers.closeSideBar();
		} else {
			if ($(window).width() > 768) {
				$(".main").removeClass("sidebar--closed");
			}
			helpers.showSideBar();
		}
	});

	$(document).on("click", ".menu-list li a", function (e) {
		if ($(this).attr("id") == "random") {
			return -1;
		}
		$("#export").val($(this).attr("id"));
	});

	setTimeout(function () {
		ajax.print({ action: "getAll", all: "all" });
	}, 200);

	$(document).on("click", "#all", function (e) {
		helpers.addActive($(this));
		let params = { action: "getAll", all: "all" };
		ajax.print(params);
	});

	$(document).on("click", "#def, #maybe", function (e) {
		helpers.addActive($(this));
		let params = { action: "getByType", type: $(this).data("receipttype") };
		ajax.print(params);
	});

	$(document).on("click", "#awarded, #rejected", function (e) {
		helpers.addActive($(this));
		let params = { action: "getByStatus", status: $(this).data("receiptstatus") };
		ajax.print(params);
	});

	$(document).on("click", "#rewards", function (e) {
		helpers.addActive($(this));
		let params = { action: "getPrizes" };
		ajax.print(params);
	});

	$(document).on("click", "#random", function (e) {
		helpers.addActive($(this));
		let params = { action: "getRandom" };
		ajax.print(params);
	});

	$(document).on("click", ".rejectAction", function (e) {
		$(this).parents(".flip").addClass("toggleCard");
		$(this).parents(".flip").children(".theFront").css({ pointerEvents: "none" });
		$(this).parents(".flip").children(".theBack").css({ pointerEvents: "all" });
	});

	$(document).on("click", ".flipBack", function (e) {
		$(this).parents(".flip").removeClass("toggleCard");
		$(this).parents(".flip").children(".theFront").css({ pointerEvents: "all" });
		$(this).parents(".flip").children(".theBack").css({ pointerEvents: "none" });
	});

	$(document).on("click", ".tempRemove", function (e) {
		let id = $(this).parents(".column").attr("id");
		let params = { action: "reject", msg: "Card moved to rejected Tab", id: id };
		ajax.proccessAction(params);
	});

	$(document).on("click", ".permRemove", function (e) {
		let id = $(this).parents(".column").attr("id");
		let params = {
			action: "permRemove",
			msg: "Card permanently removed, this action is irreversible",
			id: id,
		};
		ajax.proccessAction(params);
	});

	$(document).on("click", ".restoreAction", function (e) {
		let id = $(this).parents(".column").attr("id");
		let params = { action: "restoreRejected", msg: "Card restored to pending tab", id: id };
		ajax.proccessAction(params);
	});

	$(document).on("click", ".awardAction", function (e) {
		localStorage.setItem("currentAwardingProcces", $(this).parents(".column").attr("id"));
		ajax.printInModal($(this).parents(".column").attr("id"));
		ajax.printPrizesInModal();
		helpers.animateCSS("#stepOne", "backInDown");
	});

	$(document).on("click", "#proceedToPrizes", function (e) {
		helpers.animateCSS("#stepOne", "backOutLeft").then((smth) => {
			$("#rewardImageContainer").html(``);
			$("#stepOne").hide();
			$("#stepTwo").css({ display: "flex" });
			helpers.animateCSS("#stepTwo", "backInRight");
		});
	});

	$(document).on("click", "#givePrize", function (e) {
		let prize_id = $("#rewardsDropdown").val();
		let id = $(e.target).attr("awardid");
		if (prize_id == 0) {
			$("#rewardsDropdown").addClass("myError");
			$(".dropdownErrorMsg").css({ display: "block" });
			return -1;
		}
		ajax.proccessAction({
			action: "giveReward",
			msg: "Card awarded and moved to awarded tab",
			id: id,
			prize_id: prize_id,
		});
		localStorage.removeItem("currentAwardingProcces");
		$("#stepTwo").addClass("animate__backOutLeft");
		helpers.animateCSS("#stepTwo", "backOutLeft").then((smth) => {
			$("#stepTwo").hide();
			$("html").css({ overflow: "auto" });
			$(".modal").hide();
		});
	});

	$("#rewardsDropdown").on("change", function (e) {
		if ($("#rewardsDropdown").hasClass("myError")) {
			$("#rewardsDropdown").removeClass("myError");
		}
		$(".dropdownErrorMsg").css({ display: "none" });
	});

	$("#rewardsDropdown").on("change", function (e) {
		let imgUrl = $(this).find(":selected").data("imgurl");
		if (imgUrl) {
			$("#rewardImageContainer").html(`<img src="${imgUrl}" class="image">`);
		} else {
			$("#rewardImageContainer").html(``);
		}
	});

	$(document).on("click", ".modalClose, .modal-background", function (e) {
		$(".modal").hide();
		if (localStorage.getItem("currentAwardingProcces")) {
			localStorage.removeItem("currentAwardingProcces");
		}
		$("html").css({ overflow: "auto" });
	});
});
