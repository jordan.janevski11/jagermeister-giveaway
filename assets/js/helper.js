export function restrictUser() {
	const now = new Date();

	const item = {
		expiry: now.getTime() + 86400000,
	};
	localStorage.setItem("restricted", JSON.stringify(item));
}

export function isRestricted() {
	const itemStr = localStorage.getItem("restricted");

	if (!itemStr) {
		return false;
	}

	const item = JSON.parse(itemStr);
	const now = new Date();

	if (now.getTime() > item.expiry) {
		localStorage.removeItem("restricted");
		localStorage.removeItem("attempts");
		$("mySubmitBtn").removeAttr("disabled");
		$("#mySubmitBtn").css({ border: "none" });

		return false;
	} else {
		$("#mySubmitBtn").attr("disabled", true);
		$("#mySubmitBtn").css({ border: "3px solid red" });
		return true;
	}
}

let slidePosition = 0;
export function slideShow() {
	let slides = $(".carouselItem");
	if (slidePosition > slides.length - 1) slidePosition = 0;
	for (let i = 0; i < slides.length; i++) {
		$(slides[i]).css({ display: "none", opacity: 0 });
	}
	$(slides[slidePosition]).css({ display: "flex" }).animate({ opacity: 1 }, 2000);
	slidePosition++;
	setTimeout(slideShow, 7500);
}

export function validateEmail(email) {
	let pattern = /^(?=[^@]{3,}@)[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	if (email.match(pattern)) {
		return true;
	} else {
		return false;
	}
}

export function validateFile(fileObj) {
	if (
		fileObj.type != "image/jpeg" &&
		fileObj.type != "image/png" &&
		fileObj.type != "image/svg+xml"
	) {
		return false;
	} else {
		if (fileObj.size > 4 * 1024 * 1024) {
			return false;
		} else {
			return true;
		}
	}
}
export function clearErrors() {
	$("#emailErrorMsg").text("");
	$("#imageErrorMsg").text("");
	removeErrorClass($("#email"));
	removeErrorClass($("#fakeImageInput"));
}

function removeErrorClass(jQueryElement) {
	if (jQueryElement.hasClass("myError")) {
		jQueryElement.removeClass("myError");
	}
}

export function closeSideBar() {
	$(".menu-text").css({ display: "none", opacity: 0 });
	$(".main").addClass("sidebar--closed");
	$(".fa-angle-double-left").hide();
	$(".fa-angle-double-right").show();
	setTimeout(function () {
		$(".mainFlip").css({ height: $(".card").height() });
		$(".menu-label").animate({ opacity: 0 }, 100, function (e) {
			$(".menu-label").hide();
		});
	}, 200);
}

export function showSideBar() {
	$(".fa-angle-double-left").show();
	$(".fa-angle-double-right").hide();
	setTimeout(function () {
		$(".menu-text").show().animate({ opacity: 1 }, 500);
		$(".menu-label").show().animate({ opacity: 1 }, 100);
	}, 200);
}

export function createCard(databaseRow) {
	return $(`
	<div id="${databaseRow.id}" class="column is-half-tablet is-one-third-desktop is-one-quarter-fullhd">
		<div class="mainFlip">
			<div class="flip">
				<div class="theFront">
					<div class="card">	
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="${databaseRow.url}" alt="receipt image">
							</figure>
						</div>
						<div class="card-content">
							<div class="content">
								<p>${databaseRow.email}</p>
								<p>${databaseRow.info}</p>
							</div>
						</div>
						<div class="card-footer buttons has-addons ">
							<a class="card-footer-item button mb-0 is-link awardAction">Award</a>
							<a class="card-footer-item button mb-0 is-danger rejectAction">Reject</a>
						</div>
					</div>	
				</div>	
				<div class="theBack">	
				<div>
					<div class="button is-warning mx-5 my-5 is-block tempRemove">Move to rejected tab</div>
					<div class="button is-danger mx-5 is-block permRemove">Permanently remove</div>
				</div>			
					
					<div class="button flipBack" >Flip Back</div>
				</div>
			</div>	
		</div>	
	</div>`);
}

export function createAwardedCard(databaseRow) {
	return $(`
	<div
		id="${databaseRow.id}"
		class="column is-half-tablet is-one-third-desktop is-one-quarter-fullhd"
	>
		<div class="card">
			<div class="card-image">
				<figure class="image is-4by3">
					<img src="${databaseRow.url}" />
				</figure>
			</div>
			<div class="card-content">
				<div class="content">
					<p>${databaseRow.email}</p>
					<p>${databaseRow.info}</p>
				</div>
			</div>
			<div class="card-footer buttons has-addons ">
				<a class="card-footer-item button mb-0 is-info">${databaseRow.prize_name}</a>
			</div>
		</div>
	</div>`);
}

export function createRejectedCard(databaseRow) {
	return $(`
	<div
		id="${databaseRow.id}"
		class="column is-half-tablet is-one-third-desktop is-one-quarter-fullhd"
	>
		<div class="card">
			<div class="card-image">
				<figure class="image is-4by3">
					<img src="${databaseRow.url}" />
				</figure>
			</div>
			<div class="card-content">
				<div class="content">
					<p>${databaseRow.email}</p>
					<p>${databaseRow.info}</p>
				</div>
			</div>
			<div class="card-footer buttons has-addons ">
					<a class="card-footer-item button mb-0 is-primary restoreAction">Restore</a>
					<a class="card-footer-item button mb-0 is-danger permRemove">Delete</a>
			</div>
		</div>
	</div>`);
}

export function createPrizeCard(databaseRow) {
	return $(`<div id="${databaseRow.id}" class="column is-half-tablet is-one-third-desktop is-one-quarter-fullhd">
	<div class="card">
		<div class="card-image">
			<figure class="image is-4by3">
				<img src="${databaseRow.image}">
			</figure>
		</div>				
		<div class="card-footer">
		<p class="card-footer-item">${databaseRow.name} | quantity: ${databaseRow.quantity} </p>
	</div>
	</div>
   
</div>`);
}

export function addActive(element) {
	$("a").removeClass("is-active");
	element.addClass("is-active");
}

export function resetForm() {
	$("#myForm")[0].reset();
	$(".file-name").text("");
	$("#progressBar").hide();
}

export const animateCSS = (element, animation, prefix = "animate__") =>
	new Promise((resolve, reject) => {
		const animationName = `${prefix}${animation}`;
		const node = document.querySelector(element);

		node.classList.add(`${prefix}animated`, animationName);

		function handleAnimationEnd(event) {
			event.stopPropagation();
			node.classList.remove(`${prefix}animated`, animationName);
			resolve("Animation ended");
		}

		node.addEventListener("animationend", handleAnimationEnd, { once: true });
	});

export function followUpAnimation(msg) {
	$("#responseMsg").text(msg);
	$("#participationModal").hide();
	$("#responseMsg").css({ display: "flex" });
	resetForm();
	animateCSS("#responseMsg", "bounceIn");
}

export function animateModalWithFollowUpMsg(msg) {
	animateCSS("#participationModal", "bounceOut").then((smth) => {
		followUpAnimation(msg);
	});
}
