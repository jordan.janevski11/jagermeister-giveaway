<?php
require_once __DIR__ . "./../helper/functions.php";
require_once __DIR__ . "./../Classes/Award.php";

use awardApp\Award;

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    redirect(APP_URL . "/index");
}

if (isset($_POST["email"]) && $_POST["email"] != "" && isset($_FILES['file']) && $_FILES['file'] != []) {
    $email = $_POST['email'];
    $file = $_FILES['file'];
    if (!validateEmail($email) || !validateFile($file)) {
        logger("Frontend validations breached from {$_SERVER['REMOTE_ADDR']}");
        echo 4;
        die();
    }
} else {
    logger("Frontend validations breached from {$_SERVER['REMOTE_ADDR']}");
    echo 4;
    die();
}

$target_directory = ABS_PATH . "/" . "uploads/";
$target_file = $target_directory . basename($_FILES["file"]["name"]);
$filetype = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
$randomString = generateRandomString();
$newfilename = "uploads/" . $randomString  . "." . $filetype;

$path = APP_URL . "/" . $newfilename;

if (Award::createNew(['email' => $email, 'url' => $newfilename])) {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], ABS_PATH . "/" . $newfilename)) {
        $curl = curl_init();
        $url = "https://jager.brainster.xyz/api?img={$path}";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        $decodedOutput = json_decode($output, true);

        if (!$decodedOutput || $decodedOutput['code'] != 200) {
            if (Award::deleteRowByUrl($newfilename) > 0) {
                logger("Issue with the api call");
                unlink(ABS_PATH . "/" . $newfilename);
                echo 2;
            }
        } else if ($decodedOutput['img_status'] == 0) {
            if (Award::deleteRowByUrl($newfilename) > 0) {
                unlink(ABS_PATH . "/" . $newfilename);
                echo 0;
            }
        } else {
            if (Award::setTypeAndInfo(["type" => $decodedOutput['img_status'], "info" => $decodedOutput['text']], $randomString)) {
                echo 1;
            }
        }
    } else {
        logger("Issue with move_uploaded_file");
        echo 3;
    }
} else {
    logger("Issue with the database");
    echo 2;
}
