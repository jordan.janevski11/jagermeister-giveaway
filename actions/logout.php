<?php

require_once __DIR__ . "./../helper/functions.php";
session_start();
session_destroy();
redirect(APP_URL . "/index");
