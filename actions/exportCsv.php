<?php
session_starT();

use awardApp\Award;

require_once __DIR__ . "./../helper/functions.php";

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    redirect(APP_URL . "/dashboard");
}

require_once __DIR__ . "./../Classes/Award.php";

if (isset($_POST['action'])) {

    $headers = ['id', 'email', 'url', 'type', 'info', 'status', 'prize_id'];

    if ($_POST['action'] == "all") {
        $results = Award::selectAllPending()->fetchAll(PDO::FETCH_ASSOC);
        download($results, $headers);
    } else if ($_POST['action'] == "def") {
        $results = Award::selectByType(1)->fetchAll(PDO::FETCH_ASSOC);
        download($results, $headers);
    } else if ($_POST['action'] == "maybe") {
        $results = Award::selectByType(2)->fetchAll(PDO::FETCH_ASSOC);
        download($results, $headers);
    } else if ($_POST['action'] == "awarded") {
        array_push($headers, 'prize_name');
        $results = Award::selectByStatus(1)->fetchAll(PDO::FETCH_ASSOC);
        download($results, $headers);
    } else if ($_POST['action'] == "rejected") {
        $results = Award::selectByStatus(2)->fetchAll(PDO::FETCH_ASSOC);
        download($results, $headers);
    } else if ($_POST['action'] == "rewards") {
        $results = Award::getAllPrizes()->fetchAll(PDO::FETCH_ASSOC);
        $headers = ['id', 'name', 'image', 'quantity'];
        download($results, $headers);
    } else {
        redirect(APP_URL . "/dashboard");
    }
}

function download($results, $headers)
{
    if ($results && count($results) != 0) {
        array_to_csv_download($results, $headers);
    } else {
        $_SESSION['error'] = 'Nothing to export you were redirected to the pending tab';
        redirect(APP_URL . "/dashboard");
    }
}



function array_to_csv_download($array, $headers, $filename = "export.csv", $delimiter = ",")
{
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');

    $f = fopen('php://output', 'w');
    fputcsv($f, $headers);
    foreach ($array as $line) {
        fputcsv($f, $line, $delimiter);
    }
}
