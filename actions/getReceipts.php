<?php
session_start();

use awardApp\Award;

require_once __DIR__ . "./../helper/functions.php";

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    redirect(APP_URL . "/dashboard");
}

if (!isset($_SESSION['username'])) {
    redirect("admin");
}


require_once __DIR__ . "./../Classes/Award.php";


if (isset($_POST['action'])) {

    if ($_POST['action'] == "getAll") {
        if (isset($_POST['all'])) {
            $results = Award::selectAllPending()->fetchAll(PDO::FETCH_ASSOC);
            sendEncodedToFrontend($results);
        }
    } else

    if ($_POST['action'] == "getByType") {
        if (isset($_POST['type'])) {
            $results = Award::selectByType($_POST['type'])->fetchAll(PDO::FETCH_ASSOC);
            sendEncodedToFrontend($results);
        }
    } else

    if ($_POST['action'] == "reject") {
        if (isset($_POST['id'])) {
            @$id = decrypt($_POST['id']);
            if ($id == null) {
                echo 3;
            } else
            if (Award::setStatusById(["status" => 2], $id)) {
                echo 2;
            } else {
                echo 1;
            }
        }
    } else

    if ($_POST['action'] == "giveReward") {
        if (isset($_POST['id']) && isset($_POST['prize_id'])) {
            @$id = decrypt($_POST['id']);
            @$prizeId = decrypt($_POST['prize_id']);
            @$participantInfo = Award::selectReceiptById($id)->fetch(PDO::FETCH_ASSOC);

            if ($id == null || !$participantInfo) {
                echo 3;
                die();
            }
            $prize = Award::getPrizeById($prizeId)->fetch(PDO::FETCH_ASSOC);
            $participantEmail = $participantInfo['email'];
            $prizeName = $prize['name'];

            if (Award::setStatusById(["status" => 1, "prize_id" => $prizeId], $id)) {
                @sendMail($participantEmail, $prizeName);
                echo 2;
            } else {
                echo 1;
            }
        }
    } else

    if ($_POST['action'] == "getByStatus") {
        if (isset($_POST['status'])) {
            $results = Award::selectByStatus($_POST['status'])->fetchAll(PDO::FETCH_ASSOC);
            sendEncodedToFrontend($results);
        }
    } else

    if ($_POST['action'] == "getReceiptById") {
        if (isset($_POST['id'])) {
            @$id = decrypt($_POST['id']);
            if ($id == null) {
                echo 3;
                die();
            }
            $result = Award::selectReceiptById($id)->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                $result['id'] = encrypt($result['id']);
                sendEncodedToFrontend($result, false);
            } else {
                echo 1;
            }
        }
    } else

    if ($_POST['action'] == "getPrizes") {
        $results = Award::getAllPrizes()->fetchAll(PDO::FETCH_ASSOC);
        if ($results) {
            sendEncodedToFrontend($results);
        } else {
            echo 1;
        }
    } else

    if ($_POST['action'] == "permRemove") {
        if (isset($_POST['id'])) {
            @$id = decrypt($_POST['id']);
            if ($id == null) {
                echo 3;
                die();
            } else {
                $result = Award::selectReceiptById($id)->fetch(PDO::FETCH_ASSOC);
                if (Award::deleteRowById($id) > 0) {
                    if (unlink(ABS_PATH . "/" . $result['url'])) {
                        echo 2;
                    }
                }
            }
        }
    } else

    if ($_POST['action'] == "restoreRejected") {
        if (isset($_POST['id'])) {
            @$id = decrypt($_POST['id']);
            if ($id == null) {
                echo 3;
                die();
            }
            if (Award::setStatusById(["status" => 0], $id)) {
                echo 2;
            } else {
                echo 1;
            }
        }
    } else

    if ($_POST['action'] == "getRandom") {
        $result = Award::getRandomReceipt()->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            $result['id'] = encrypt($result['id']);
            sendEncodedToFrontend($result, false);
        } else {
            echo 1;
        }
    }
}

function sendMail($to, $prizeName)
{
    $subject = "Jagermeister Givaway";
    $message = "Congratulations!!! You have been awarded with a {$prizeName} ! For additional information contact 123-456-789";
    $headers = "From: BrainsterJagermaister@giveaway.com";
    mail($to, $subject, $message, $headers);
}


// function jsonEncode + encrypt

function encryptKeyFromArray($results)
{
    $encrypedArray = [];
    foreach ($results as $result) {
        $result['id'] = encrypt($result['id']);
        array_push($encrypedArray, $result);
    }
    return $encrypedArray;
}

function sendEncodedToFrontend($results, $encrypt = true)
{
    if ($results) {
        header('Content-type: application/json; charset=utf-8');
        header('X-Content-Type-Options: nosniff');
        if ($encrypt) {
            echo json_encode(encryptKeyFromArray($results));
        } else {
            echo json_encode($results);
        }
    } else {
        echo 1;
    }
}
