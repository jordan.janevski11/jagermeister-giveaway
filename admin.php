<?php
session_start();
require_once __DIR__ . "/helper/functions.php";

isLoggedIn();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    require_once __DIR__ . "/Classes/User.php";

    if (!\awardApp\User::login($_POST['username'], $_POST['password'])) {
        $_SESSION['error'] = "Wrong credentials";
        $_SESSION['oldUsername'] = $_POST['username'];
        redirect("admin");
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once __DIR__ . "/pageParts/cdnAndMeta.php"; ?>
    <title>Jagermester Admin Login</title>
    <link rel="stylesheet" href="./assets/css/adminLoginStyle.css" />
</head>

<body>
    <nav class="navbar is-transparent is-fixed-top py-3">
        <div class="navbar-brand">
            <a id="logo" href="index">Jägermeister</a>
        </div>
    </nav>

    <div id="login">

        <?php
        if (isset($_SESSION['error'])) {
            echo "<div class='notification is-danger absoluteNotification'> {$_SESSION['error']} </div>";
        }
        ?>
        <div>
            <form action="" method="POST">

                <div class="field">
                    <label class="label has-text-white " for="username">Username</label>
                    <input class="input <?= isset($_SESSION['error']) ? "myError" : ''  ?>" type="text" id="username" name="username" value="<?= isset($_SESSION['oldUsername']) ? $_SESSION['oldUsername'] : ''  ?>" />
                </div>

                <div class="field">
                    <label class="label has-text-white" for="password">Password</label>
                    <input class="input <?= isset($_SESSION['error']) ? "myError" : ''  ?>" type="password" id="password" name="password" />
                </div>

                <div class="field">
                    <div class="control">
                        <button class="button is-fullwidth loginBtn has-text-white" type="submit">
                            Log In
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</body>


<?php unset($_SESSION['error']); ?>
<!-- scripts and jquery -->
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

</html>