<?php require_once __DIR__ . "./../config/consts.php" ?>

<meta charset="UTF-8">
<meta name="author" content="Jordan Janevski">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">

<link rel="icon" type="image/png" href=<?= APP_URL . "/assets/images/logo.png" ?>>