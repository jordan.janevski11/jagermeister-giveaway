<?php

require_once __DIR__ . "./../config/consts.php";

function generateRandomString()
{
    $lowerCharacters = 'abcdefghijklmnopqrstuvwxyz';
    $upperCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $numbers = '0123456789';
    $randomString = '';

    for ($i = 0; $i < 2; $i++) {
        $index1 = rand(0, strlen($lowerCharacters) - 1);
        $index2 = rand(0, strlen($numbers) - 1);
        $index3 = rand(0, strlen($upperCharacters) - 1);
        $randomString .= $lowerCharacters[$index1] . $numbers[$index2] . $upperCharacters[$index3];
    }
    return $randomString;
}

function validateEmail($email)
{
    $sanitized = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($sanitized, FILTER_VALIDATE_EMAIL)) {
        if (!preg_match("/[a-zA-Z\d\._-]{3}+@/", $email)) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function validateFile($file)
{
    $allowedExts = array("jpeg", "jpg", "png", "svg");
    $tmp = explode(".", $file['name']);
    $extension = end($tmp);

    if (!in_array($extension, $allowedExts)) {
        return false;
    }
    if ($file['size'] > 4 * 1024 * 1024 && $file['error'] != 0) {

        return false;
    } else {
        return true;
    }
}

function redirect($url)
{
    header("location: $url");
    die();
}

function isLoggedIn()
{
    if (isset($_SESSION['username'])) {
        redirect("dashboard");
    }
}

function logger($msg)
{
    file_put_contents(ABS_PATH . "/storage/log.txt", "[" . date("Y-m-d H:i:s") . "]: " . $msg . PHP_EOL, FILE_APPEND);
}



$encryptionKey = ENCRYPTION_KEY;

function encrypt($plaintext)
{
    global $encryptionKey;
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $encryptionKey, $options = OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $encryptionKey, $as_binary = true);
    $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);

    return $ciphertext;
}


function decrypt($ciphertext)
{
    global $encryptionKey;
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len = 32);
    $ciphertext_raw = substr($c, $ivlen + $sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $encryptionKey, $options = OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $encryptionKey, $as_binary = true);
    if (hash_equals($hmac, $calcmac)) // timing attack safe comparison
    {
        return $original_plaintext;
    }
}
