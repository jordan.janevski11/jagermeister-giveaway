<?php

namespace awardApp;

use PDO;

require_once __DIR__ . "/DB.php";

class Award
{

    protected static $table = "participations";
    protected static $tableWithPrizes = "prizes";

    public static function createNew($array)
    {
        return DB::insert(self::$table, $array);
    }

    public static function deleteRowByUrl($url)
    {
        $condition = "url=$url";
        return DB::delete(self::$table, $condition);
    }

    public static function deleteRowById($id)
    {
        $condition = "id=$id";
        return DB::delete(self::$table, $condition);
    }

    public static function setTypeAndInfo($array, $condition)
    {
        $condition = "url LIKE '%$condition%'";
        return DB::update(self::$table, $array, $condition);
    }

    public static function selectByType($type)
    {
        $table = self::$table;
        $sql = "SELECT * FROM $table WHERE type=:type AND status=0";
        return DB::select($sql, ['type' => $type]);
    }
    public static function selectByStatus($status)
    {
        $table = self::$table;
        $prizes = self::$tableWithPrizes;
        $sql = "SELECT $table.*, $prizes.name AS prize_name FROM $table INNER JOIN $prizes ON prize_id = $prizes.id WHERE status=:status";
        if ($status == 2) {
            $sql = "SELECT * FROM $table WHERE status=:status";
        }
        return DB::select($sql, ['status' => $status]);
    }
    public static function selectReceiptById($id)
    {
        $table = self::$table;
        $sql = "SELECT * FROM $table WHERE id=:id";
        return DB::select($sql, ['id' => $id]);
    }

    public static function selectAllPending()
    {
        $table = self::$table;
        $sql = "SELECT * FROM $table WHERE status=0";
        return DB::select($sql);
    }

    public static function setStatusById($array, $condition)
    {
        $condition = "id=$condition";
        if (isset($array['prize_id'])) {
            $prize = self::getPrizeById($array['prize_id'])->fetchAll(PDO::FETCH_ASSOC);
            self::updateItemQuantity(['quantity' => $prize[0]['quantity'] - 1], $array['prize_id']);
        }
        return DB::update(self::$table, $array, $condition);
    }

    public static function getAllPrizes()
    {
        $table = self::$tableWithPrizes;
        $sql = "SELECT * FROM $table";
        return DB::select($sql);
    }

    public static function getPrizeById($id)
    {
        $table = self::$tableWithPrizes;
        $sql = "SELECT * FROM $table WHERE id=:id";
        return DB::select($sql, ['id' => $id]);
    }

    public static function updateItemQuantity($array, $condition)
    {
        $table = self::$tableWithPrizes;
        $condition = "id=$condition";
        return DB::update($table, $array, $condition);
    }

    public static function getRandomReceipt()
    {
        $table = self::$table;
        $sql = "SELECT * FROM $table WHERE status = 0 ORDER BY RAND() LIMIT 1";
        return DB::select($sql);
    }
}
