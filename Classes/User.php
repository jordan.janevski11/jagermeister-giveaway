<?php

namespace awardApp;

require_once __DIR__ . "/DB.php";

class User
{

    protected static $table = "admins";

    public static function login($username, $password)
    {
        $table = self::$table;
        $sql = "SELECT * FROM $table WHERE username = :username";
        $user = DB::select($sql, ['username' => $username])->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            return false;
        }

        if (password_verify($password, $user['password'])) {
            $_SESSION['username'] = $user['username'];
            header("location: dashboard");
            die();
        } else {
            return false;
        }
    }
}
