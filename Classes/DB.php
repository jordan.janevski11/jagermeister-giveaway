<?php

namespace awardApp;

require_once __DIR__ . "./../config/consts.php";

use PDO;
use PDOException;

class DB
{

    private static $instance = null;

    public function __construct()
    {
        try {
            self::$instance = new PDO("mysql:" . "host=" . DB_HOST . ";dbname=" . DB_NAME . ";", DB_USER, DB_PASS);
        } catch (PDOException $e) {
            logger($e->getMessage());
            echo "Database down";
        }
    }

    public static function connect()
    {
        if (is_null(self::$instance)) {
            new self();
        }
        return self::$instance;
    }

    public static function insert($table, $array)
    {

        $db = self::connect();
        $cols = [];

        foreach ($array as $key => $value) {
            $cols[$key] = $key;
        }

        $columns = implode(",", $cols);

        $placeholders = "";

        foreach ($cols as $placeholder) {
            $placeholders .= ":$placeholder,";
        }

        $placeholders = rtrim($placeholders, ",");

        $sql = "INSERT INTO $table ($columns) VALUES ($placeholders)";

        $stmt = $db->prepare($sql);

        return $stmt->execute($array);
    }

    public static function select($sql, array $conditionValue = null)
    {
        $db = self::connect();

        $stmt = $db->prepare($sql);
        $stmt->execute($conditionValue);
        return $stmt;
    }

    public static function update($table, $array, $condition)
    {
        $db = self::connect();

        $placeholders = "";

        foreach ($array as $key => $value) {
            $placeholders .= "$key=:$key,";
        }

        $placeholders = rtrim($placeholders, ",");

        $sql = "UPDATE {$table} SET {$placeholders} WHERE $condition";

        $stmt = $db->prepare($sql);

        return $stmt->execute($array);
    }


    public static function delete($table, $condition)
    {
        $db = self::connect();
        $conditionSpreaded = explode("=", $condition);
        $condition = $conditionSpreaded[0] . "=:$conditionSpreaded[0]";
        $conditionValue = $conditionSpreaded[1];
        $sql = "DELETE FROM {$table} WHERE $condition";

        $stmt = $db->prepare($sql);
        $stmt->bindValue("$conditionSpreaded[0]", $conditionValue);

        $stmt->execute();
        return $stmt->rowCount();
    }
}
