<?php session_start();

require_once __DIR__ . "/helper/functions.php";

if (!isset($_SESSION['username'])) {
    redirect("admin");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once __DIR__ . "/pageParts/cdnAndMeta.php" ?>
    <title>Admin Panel</title>
    <link rel="stylesheet" href="./assets/css/dashboardStyle.css">
</head>

<body>
    <header class="hero">
        <div class="hero-head">

            <nav class="navbar is-mobile">
                <div class="navbar-brand">
                    <a class="navbar-item" id="logo" href="index">
                        Jägermeister
                    </a>
                    <a class="navbar-item ml-auto">
                        <?= $_SESSION['username']  ?>
                    </a>
                    <a class="navbar-item" href="actions/logout.php">
                        Logout
                    </a>


                    <?php
                    if (isset($_SESSION['error'])) {
                        echo "<div class='notification is-danger autoHide'> {$_SESSION["error"]} </div>";
                        unset($_SESSION['error']);
                    }
                    ?>

                    <div class="successNotification notification is-success">

                    </div>
                    <div class="errorNotification notification is-danger">

                    </div>
                </div>

            </nav>

        </div>

        <hr>

    </header>


    <div class="modal">
        <div class="modal-background"></div>

        <div class="modal-card" id="stepOne">
            <header class="modal-card-head">
                <p class="modal-card-title"></p>
                <button class="delete modalClose" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <!-- Content ... -->
            </section>
            <footer class="modal-card-foot">
                <button id="proceedToPrizes" class="button is-success">Pick a prize</button>
                <button class="button modalClose">Cancel</button>
            </footer>
        </div>

        <div class="modal-card" id="stepTwo">
            <header class="modal-card-head">
                <p class="modal-card-title"></p>
                <button class="delete modalClose" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div id="rewardImageContainer">

                </div>
                <select id="rewardsDropdown" class="select is-fullwidth">

                </select>
                <p class="dropdownErrorMsg mb-0">Please pick a reward.</p>
            </section>
            <footer class="modal-card-foot">
                <button id="givePrize" class="button is-success">Award</button>
                <button class="button modalClose">Cancel</button>
            </footer>
        </div>


    </div>

    <div class="wrapper">

        <aside id="main-sidebar" class="aside">
            <p class="menu-label">
                Receipts
            </p>
            <ul class="menu-list">
                <li><a id="all" class="is-active"><span class="icon is-small"><i class="fas fa-hourglass-half"></i></span><span class="menu-text">Pending</span></a></li>
                <li><a id="def" data-receiptType="1"><span class="icon is-small"><i class="fas fa-check-double"></i></span><span class="menu-text">Definitly a receipt</span></a></li>
                <li><a id="maybe" data-receiptType="2"><span class="icon is-small"><i class="fas fa-check"></i></span><span class="menu-text">Maybe a receipt</span></a></li>
                <li><a id="random"><span class="icon is-small"><i class="fas fa-dice"></i></span><span class="menu-text">Random</span></a></li>

            </ul>

            <p class="menu-label mt-5">
                Concluded
            </p>
            <ul class="menu-list">
                <li><a id="awarded" data-receiptStatus="1"><span class="icon is-small"><i class="fas fa-award"></i></span><span class="menu-text">Awarded</span></a></li>
                <li><a id="rejected" data-receiptStatus="2"><span class="icon is-small"><i class="fas fa-times"></i></span><span class="menu-text">Rejected</span></a></li>
            </ul>

            <p class="menu-label mt-5">
                Rewards
            </p>
            <ul class="menu-list">
                <li><a id="rewards"><span class="icon is-small"><i class="fas fa-gifts"></i></span><span class="menu-text">Rewards</span></a></li>
            </ul>

            <p class="menu-label mt-5">
                Export
            </p>
            <ul class="menu-list">
                <li>
                    <form id="csvForm" action="./actions/exportCsv.php" method="POST">
                        <input type="hidden" name="action" id="export" value='all'>
                        <button class="button is-dark downlaodButton" type="submit"><span class="icon is-small"><i class="fas fa-file-export"></i></span><span class="menu-text has-text-white">Exort as CSV</span></button>
                    </form>
                </li>

            </ul>

            <div id="sidebar-toggler">
                <span class="icon is-small"><i class="fa fa-angle-double-left"></i></span>
                <span class="icon is-small"><i class="fa fa-angle-double-right"></i></span>
            </div>
        </aside>


        <div class="main">
            <div id="preview" class="columns is-multiline"></div>
        </div>
    </div>




    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script type="module" src="./assets/js/dashboard.js"></script>
</body>

</html>