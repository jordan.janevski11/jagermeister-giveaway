<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once __DIR__ . "/pageParts/cdnAndMeta.php"; ?>
    <title>Jagermester</title>
    <link rel="stylesheet" href="./assets/css/indexStyle.css" />
</head>

<body>
    <div class="modal">
        <div class="modal-background"></div>
        <div id="participationModal" class="modal-content p-6">
            <form id="myForm">
                <div class="field">
                    <label class="label has-text-white" for="email">Email</label>

                    <div class="control has-icons-left has-icons-right">
                        <input class="input" type="email" id="email" name="email" placeholder="Email input" />
                        <span class="icon is-small is-left">
                            <i class="fas fa-envelope"></i>
                        </span>
                    </div>
                </div>
                <p class="help is-danger mb-3" id="emailErrorMsg"></p>

                <div class="field">
                    <div class="file has-name is-fullwidth">
                        <label class="file-label">
                            <input class="file-input" type="file" name="image" id="image" />
                            <span class="file-cta" id="fakeImageInput">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">Choose a file…</span>
                            </span>
                            <span class="file-name"></span>
                        </label>
                    </div>
                </div>
                <p class="help is-danger mb-3" id="imageErrorMsg"></p>

                <div class="field">
                    <div id="progressBar">
                        <div id="progress">

                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <button class="button is-fullwidth jjButton has-text-white" type="submit" id="mySubmitBtn">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="modal-content p-6" id="responseMsg">
        </div>


        <button class="modal-close is-large" aria-label="close"></button>
    </div>

    <nav class="navbar is-transparent is-fixed-top px-6 py-3">
        <div class="navbar-brand">
            <a id="logo" href="index">Jägermeister</a>
            <a class="navbar-item" href="admin">Admin</a>
        </div>
    </nav>

    <section class="hero is-fullheight">
        <div class="columns" id="jjContainer">
            <div id="left" class="column is-two-thirds-tablet">
                <div class="column is-two-thirds-tablet has-text-left" id="innerLeft">
                    <h1 class="is-size-1 has-text-weight-semibold mb-3 has-text-white">
                        Jägermeister givaway
                    </h1>
                    <p>
                        Das ist des Jägers Ehrenschild, daß er beschützt und hegt sein Wild, weidmännisch jagt, wie sich's gehört, den Schöpfer im Geschöpfe ehrt.
                    </p>
                    <div class="mt-5">
                        <a id="participate" class="button is-large jjButton jjHover p-5 has-text-white">Participate</a>
                    </div>
                </div>

                <div class="socialMedia">
                    <a class="myLink" href="https://www.facebook.com/JagermeisterPanama/"><i class="fab fa-3x fa-facebook"></i></a>
                    <a class="myLink" href="https://twitter.com/JagermeisterUSA"><i class="fab fa-3x fa-twitter"></i></a>
                    <a class="myLink" href="https://www.instagram.com/jagermeister/"><i class="fab fa-3x fa-instagram"></i></a>
                </div>
            </div>
            <div id="right" class="column is-one-third-tablet">
                <div class="carouselItem">
                    <img src="./assets/images/bottle1.png" alt="" />
                </div>
                <div class="carouselItem">
                    <img src="./assets/images/bottle2.png" alt="" />
                </div>
                <div class="carouselItem">
                    <img src="./assets/images/bottle3.png" alt="" />
                </div>
            </div>
        </div>
    </section>

</body>



<!-- scripts and jquery -->
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script type="module" src="./assets/js/app.js"></script>

</html>