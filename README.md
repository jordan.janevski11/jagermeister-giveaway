<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/jordan.janevski11/brainster-project02-jordanjanevski">
    <img src="assets/images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Jagermeister giveaway</h3>

  <p align="center">    
    <br />
    <a href="https://gitlab.com/jordan.janevski11/brainster-project02-jordanjanevski"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://jordanjanevski.amd.bointsoft.com">View Site</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>   
    <li><a href="#contributing">Contributing</a></li>    
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

A simple web app used for giveaways. Consists of 3 pages, a client side page where users can send photos of their receipts and dashboard where admin users can decide on who wins and who doesnt.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

- [PHP](https://www.php.net)
- [Bulma](https://bulma.io)
- [JQuery](https://jquery.com)
- [Braisnter's API](https://jager.brainster.xyz/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

This is how you can set up this project locally. To get a local copy up and running follow these simple example steps.

### Prerequisites

In order to be able to run this project you need to have installed

- php
  ```sh
  https://www.php.net/downloads
  ```
- apache
  ```sh
  https://www.apachelounge.com/download
  ```
- mysql
  ```sh
  https://dev.mysql.com/downloads/installer
  ```
  Or all together
- Xampp (This one includes everything together)
  ```sh
  https://www.apachefriends.org/index.html
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/jordan.janevski11/brainster-project02-jordanjanevski
   ```
2. Import the database from the project-root/database directory
3. Modify `consts.php` with your settings
   ```php
   const DB_NAME = "YOUR_DB_NAME";
   ```
4. This app has a feature of sending mails. While being on localhost you need to have a mail server installed on your machine.
   To avoid any inconveniences all the errors and warnings from the mail function have been suppressed. This step is optional ofcourse if you want your app to be fully functionable.
   ```
   Example: https://www.hmailserver.com/
   ```
   Or you can configure your php.ini and sendmail.ini files to be able to send mails from XAMPP using Gmail as done in the blog below
   ```
   https://meetanshi.com/blog/send-mail-from-localhost-xampp-using-gmail/
   ```
5. The users that are able to log in the admin panel are stored in a database table.
   Their password is hashed using the BCRYPT hash algorithm and the authentication in the code is done by a php password_verify function.
   If you want to add more users to the databese please make sure you insert the apporpirate hash inside the password field.
   If you do not wish to insert new users you can just use the credentials I provide you for simplicity's sake
   ```
   Username: admin
   Password: 123qwe!@#
   ```
6. Modify the .htaccess file to achieve fair security of your code
   .htaccess is a configuration file for use on web servers running the Apache Web Server software. It runs before the php code so making this dynamical is an option by setting enviroment variables but in this project there arent many configurations so these can be done manualy.

   The following example redirects the users that try to access the config directory via HTTP request and redirects them to the destination written in the RewriteRule

   ```
   RewriteCond %{REQUEST_URI} config
   RewriteRule ^(.*)$ https://jordanjanevski.amd.bointsoft.com/ [R=301,L]
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Jordan Janevski - jordan.janevski@live.com

Project Link: [https://gitlab.com/jordan.janevski11/brainster-project02-jordanjanevski](https://gitlab.com/jordan.janevski11/brainster-project02-jordanjanevski)

<p align="right">(<a href="#top">back to top</a>)</p>
